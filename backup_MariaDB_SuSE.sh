
#!/bin/bash
#
# CopyLeft moon.cat
# https://creativecommons.org/licenses/by/4.0/
#
# Author: Rei Izumi - moon.cat - 2020
#
# Generates a backup of the chosen file or folder.
#
# Exit codes:
# - 1: MariaDB is stopped
# - 2: MariaDB config file not found
# - 3: Error mounting the folder
# - 4: Target folder doesn't exist and it's not possible to create it
# - 5: Failed to create a backup of one or more databases
# - 6: Error sending the notification 

####################
# Config           #
####################

#Process identifier used by notification
IDENTIFIER_NAME=MariaDB

#MariaDB IP
MARIADB_IP=localhost

#MariaDB Port
MARIADB_PORT=3306

#MariaDB Config file
MARIADB_CONFIG=/opt/scripts/backup/mariadb.cnf

#MariaDB List of databases separated by space
MARIADB_DATABASES="database1 database2"

#Destination folder for the backup
BACKUP_FOLDER=/mnt/backup/$IDENTIFIER_NAME

#Number of backups to store.
#If the script path is empty, rotation will be disabled
BACKUP_MAXIMUM_FILES=7
BACKUP_ROTATION_SCRIPT=/opt/scripts/rotateFiles.sh

#Date format used in the backup filename
BACKUP_FILENAME_DATE=`date +_%Y-%m-%d_%H-%M-%S`

#Script to mount the unit at the beginning of the process
#Do not indicate any value to deactivate
AUTOMOUNT_SCRIPT=/opt/scripts/mount.sh

#Script used to send the notification, it have to accept the following parameters:
# $1: Identifier name
# $2: COMPLETED / ERROR
# $3: Subject
# $4: Message
NOTIFICATION_SCRIPT=/opt/scripts/notifyCSV.sh

#If enabled, each actions is reported on the console
LOG=true

####################
# Functions        #
####################
function notificationError
{
	if [ ! -z $NOTIFICATION_SCRIPT ]; then
		[ $LOG == true ] && echo "Error: $1 - $2"
		$NOTIFICATION_SCRIPT "$IDENTIFIER_NAME" "ERROR" "$1" "$2"
	else
        	echo "Notification script is empty. No notification will be sent"
	fi
}
function notificationCompleted
{
	if [ ! -z $NOTIFICATION_SCRIPT ]; then
		[ $LOG == true ] && printf "Completed: $1"
                $NOTIFICATION_SCRIPT "$IDENTIFIER_NAME" "COMPLETED" "Backup created" "$1"
		
		if [ $? -ne 0 ]; then
		        echo "Error sending the notification"
		        exit 6
		fi

        else
                echo "Notification script is empty. No notification will be sent"
        fi
}
function notificationCompletedWithError
{
        if [ ! -z $NOTIFICATION_SCRIPT ]; then
                [ $LOG == true ] && printf "Completed: $1"
                $NOTIFICATION_SCRIPT "$IDENTIFIER_NAME" "ERROR" "Backup created with errors" "$1"

                if [ $? -ne 0 ]; then
                        echo "Error sending the notification"
                        exit 6
                fi

        else
                echo "Notification script is empty. No notification will be sent"
        fi
}


####################
# Process          #
####################

#Check MariaDB is started
[ $LOG == true ] && echo "Checking that MariaDB is started"
nc -z -v -w5 $MARIADB_IP $MARIADB_PORT > /dev/null 2> /dev/null
if [ $? -ne 0 ]; then
	notificationError "MariaDB is stopped" "MariaDB is not found with $MARIADB_IP:$MARIADB_PORT, it may be stopped"
	exit 1
fi

[ $LOG == true ] && echo "Checking the config file"

if [ ! -f "$MARIADB_CONFIG" ]; then
        notificationError "MariaDB config file not found" "You must configure a config file with the MariaDB information"
        exit 2
fi

#Automount
if [ ! -z $AUTOMOUNT_SCRIPT ]; then
	[ $LOG == true ] && echo "Checking if the folder is mounted"

	$AUTOMOUNT_SCRIPT
	if [ $? -ne 0 ]; then
		notificationError "Automount failed" "Automount script has failed, check manually if this works "
		exit 3
	fi
fi

#Check target folder
[ $LOG == true ] && echo "Checking backup folder"

if [ ! -d "$BACKUP_FOLDER" ]; then
	[ $LOG == true ] && echo "Creating backup folder"
	mkdir -p $BACKUP_FOLDER
	if [ $? -ne 0 ]; then
                notificationError "Backup folder doesn't exist" "The folder where to store the backup does not exist and could not be created"
                exit 4
        fi
fi

#Rotate folders
if [ ! -z $BACKUP_ROTATION_SCRIPT ]; then
	[ $LOG == true ] && echo "Rotating old backups"
	finalFolder=$($BACKUP_ROTATION_SCRIPT $BACKUP_FOLDER $BACKUP_MAXIMUM_FILES)
else
	[ $LOG == true ] && echo "Rotation disabled"
	$finalFolder=$BACKUP_FOLDER
fi

[ $LOG == true ] && echo "Final folder: $finalFolder"

#Create backup
notificationMessage="Target files:"
error=false

for database in $MARIADB_DATABASES
do
	backupFilename="$finalFolder/$database$BACKUP_FILENAME_DATE.sql"
	[ $LOG == true ] && echo "Creating backup of $database on $backupFilename"
	mysqldump --defaults-file=$MARIADB_CONFIG $database > $backupFilename
 
	if [ $? -ne 0 ]; then
		notificationMessage="$notificationMessage\nERROR $backupFilename"
		error=true
	else
		notificationMessage="$notificationMessage\n$backupFilename"
	fi
done

#Notification
[ $LOG == true ] && echo "Sending notification"

if [ $error == true ]; then
	notificationCompletedWithError "$notificationMessage"
	exit 5
else
	notificationCompleted "$notificationMessage"
fi
