#!/bin/bash
#
# CopyLeft moon.cat
# https://creativecommons.org/licenses/by/4.0/
#
# Author: Rei Izumi - moon.cat - 2020
#
# Service to send e-mails.
# The server must have the configuration to send e-mails.
#
# $1: Identifier name
# $2: COMPLETED / ERROR
# $3: Subject
# $4: Message
#

###############
# CONFIG      #
###############

#User or group that will receive the e-mail
MAIL_TO=rei.izumi@moon.cat

#User sending the e-mail
MAIL_SENDER="Backup <backup@moon.cat>"

#These data will be added to the subject
SERVER_ENVIRONMENT=DEV
SERVER_NAME=MioAkiyama7

###############
# PROCESS     #
###############

SUBJECT="$2 - $SERVER_ENVIRONMENT/$SERVER_NAME - $1 - $3"

printf "$4" | mail -s "$SUBJECT" -r "$MAIL_SENDER" $MAIL_TO
