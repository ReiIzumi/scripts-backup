#!/bin/bash
#
# CopyLeft moon.cat
# https://creativecommons.org/licenses/by/4.0/
#
# Author: Rei Izumi - moon.cat - 2020
#
# This process mounts a NFS shared folder if it is not currently mounted
#

###############
# CONFIG      #
###############

NFS_SERVER="nas.domain.intranet"
NFS_FOLDER="/BKP_Test"
MOUNT_FOLDER="/mnt/backup"

###############
# PROCESS     #
###############

function nfsMount
{
	echo "Mounting shared folder"
	mount -t nfs $NFS_SERVER:$NFS_FOLDER $MOUNT_FOLDER
}

#Check if the mount folder exist
if [ ! -d $MOUNT_FOLDER ]; then
	echo "Creating mount folder"
	mkdir -p $MOUNT_FOLDER
	
	#Mount folder
	nfsMount
else 
	#Check if shared folder is mounted
	if [[ `mount -l | grep $NFS_SERVER:$NFS_FOLDER | grep $MOUNT_FOLDER | wc -l` -ne 1 ]]; then
		#Mount folder
		nfsMount
	fi
fi

