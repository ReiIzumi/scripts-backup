#!/bin/bash
#
# CopyLeft moon.cat
# https://creativecommons.org/licenses/by/4.0/
#
# Author: Rei Izumi - moon.cat - 2020
#
# Generates a backup of the chosen file or folder.
#
# Exit codes:
# - 1: DB2 is stopped
# - 2: Error mounting the folder
# - 3: Target folder doesn't exist and it's not possible to create it
# - 4: Failed to create a backup of one or more databases
# - 5: Error sending the notification 

####################
# Config           #
####################

#Process identifier used by notification
IDENTIFIER_NAME=DB2

#DB2 Port - Only used to check if the service is started
DB2_PORT=50001

#DB2 User
DB2_USER=db2inst1

#DB2 List of databases separated by space, each one can have the schema to export, if empty all schemas will be exported, according to the format:
# dbName:schemaName
#Example of exporting first database with schema and another 2 without schema:
# dbName1:schemaName1 dbName2 dbName3:
DB2_DATABASES="dbName1 dbName2:schema2"

#Destination folder for the backup
BACKUP_FOLDER=/mnt/backup/$IDENTIFIER_NAME

#Number of backups to store.
#If the script path is empty, rotation will be disabled
BACKUP_MAXIMUM_FILES=7
BACKUP_ROTATION_SCRIPT=/opt/scripts/rotateFiles.sh

#Date format used in the backup filename
BACKUP_FILENAME_DATE=`date +_%Y-%m-%d_%H-%M-%S`

#Script to mount the unit at the beginning of the process
#Do not indicate any value to deactivate
AUTOMOUNT_SCRIPT=/opt/scripts/mount.sh

#Script used to send the notification, it have to accept the following parameters:
# $1: Identifier name
# $2: COMPLETED / ERROR
# $3: Subject
# $4: Message
NOTIFICATION_SCRIPT=/opt/scripts/notifyCSV.sh

#If enabled, each actions is reported on the console
LOG=true

####################
# Functions        #
####################
function notificationError
{
	if [ ! -z $NOTIFICATION_SCRIPT ]; then
		[ $LOG == true ] && echo "Error: $1 - $2"
		$NOTIFICATION_SCRIPT "$IDENTIFIER_NAME" "ERROR" "$1" "$2"
	else
        	echo "Notification script is empty. No notification will be sent"
	fi
}
function notificationCompleted
{
	if [ ! -z $NOTIFICATION_SCRIPT ]; then
		[ $LOG == true ] && printf "Completed: $1"
                $NOTIFICATION_SCRIPT "$IDENTIFIER_NAME" "COMPLETED" "Backup created" "$1"
		
		if [ $? -ne 0 ]; then
		        echo "Error sending the notification"
		        exit 5
		fi

        else
                echo "Notification script is empty. No notification will be sent"
        fi
}
function notificationCompletedWithError
{
        if [ ! -z $NOTIFICATION_SCRIPT ]; then
                [ $LOG == true ] && printf "Completed: $1"
                $NOTIFICATION_SCRIPT "$IDENTIFIER_NAME" "ERROR" "Backup created with errors" "$1"

                if [ $? -ne 0 ]; then
                        echo "Error sending the notification"
                        exit 5
                fi

        else
                echo "Notification script is empty. No notification will be sent"
        fi
}

function db2Export
{
	if [ -z "$3" ]; then
		su - $DB2_USER -c "mkdir $1 && cd $1 && db2move $2 EXPORT -aw"
	else
		su - $DB2_USER -c "mkdir $1 && cd $1 && db2move $2 EXPORT -sn $3 -aw"
	fi
}

####################
# Process          #
####################

#Check DB2 is started
[ $LOG == true ] && echo "Checking that DB2 is started"
nc -z -v -w5 localhost $DB2_PORT > /dev/null 2> /dev/null
if [ $? -ne 0 ]; then
	notificationError "DB2 is stopped" "No services is found in the port $DB2_PORT, it may be stopped"
	exit 1
fi

#Automount
if [ ! -z $AUTOMOUNT_SCRIPT ]; then
	[ $LOG == true ] && echo "Checking if the folder is mounted"

	$AUTOMOUNT_SCRIPT
	if [ $? -ne 0 ]; then
		notificationError "Automount failed" "Automount script has failed, check manually if this works "
		exit 2
	fi
fi

#Check target folder
[ $LOG == true ] && echo "Checking backup folder"

if [ ! -d "$BACKUP_FOLDER" ]; then
	[ $LOG == true ] && echo "Creating backup folder"
	mkdir -p $BACKUP_FOLDER
	if [ $? -ne 0 ]; then
                notificationError "Backup folder doesn't exist" "The folder where to store the backup does not exist and could not be created"
                exit 3
        fi
fi

#Rotate folders
if [ ! -z $BACKUP_ROTATION_SCRIPT ]; then
	[ $LOG == true ] && echo "Rotating old backups"
	finalFolder=$($BACKUP_ROTATION_SCRIPT $BACKUP_FOLDER $BACKUP_MAXIMUM_FILES)
else
	[ $LOG == true ] && echo "Rotation disabled"
	$finalFolder=$BACKUP_FOLDER
fi

[ $LOG == true ] && echo "Final folder: $finalFolder"

#Assign the folder to the db2 user
chown $DB2_USER $finalFolder

#Create backup
notificationMessage="Target files:"
error=false

for database in $DB2_DATABASES
do
	dbName=`echo $database | cut -d : -f -1`
	backupFolder="$finalFolder/$dbName$BACKUP_FILENAME_DATE"
	[ $LOG == true ] && echo "Creating backup of $dbName on $backupFolder"

	if [[ $database == *":"* ]]; then
		dbSchema=`echo $database | cut -d : -f 2-`
		db2Export $backupFolder $dbName $dbSchema
	else
		db2Export $backupFolder $dbName ""
	fi

	if [ $? -ne 0 ]; then
		notificationMessage="$notificationMessage\nERROR $backupFolder"
		error=true
	else
		notificationMessage="$notificationMessage\n$backupFolder"
	fi
done

#Notification
[ $LOG == true ] && echo "Sending notification"

if [ $error == true ]; then
	notificationCompletedWithError "$notificationMessage"
	exit 4
else
	notificationCompleted "$notificationMessage"
fi
