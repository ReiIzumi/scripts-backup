#!/bin/bash
#
# CopyLeft moon.cat
# https://creativecommons.org/licenses/by/4.0/
#
# Author: Rei Izumi - moon.cat - 2020
#
# Generates a backup of the chosen file or folder.
#
# Exit codes:
# - 1: PostgreSQL is stopped
# - 2: Error mounting the folder
# - 3: Target folder doesn't exist and it's not possible to create it
# - 4: Failed to create a backup of one or more databases
# - 5: Error sending the notification 

####################
# Config           #
####################

#Process identifier used by notification
IDENTIFIER_NAME=PostgreSQL

#PostgreSQL Port - Only used to check if the service is started
POSTGRESQL_PORT=5432

#PostgreSQL User
POSTGRESQL_USER=postgres

#PostgreSQL List of databases separated by space
POSTGRESQL_DATABASES="dbName1 dbName2"

#Destination folder for the backup
BACKUP_FOLDER=/mnt/backup/$IDENTIFIER_NAME

#Number of backups to store.
#If the script path is empty, rotation will be disabled
BACKUP_MAXIMUM_FILES=7
BACKUP_ROTATION_SCRIPT=/opt/scripts/rotateFiles.sh

#Date format used in the backup filename
BACKUP_FILENAME_DATE=`date +_%Y-%m-%d_%H-%M-%S`

#Script to mount the unit at the beginning of the process
#Do not indicate any value to deactivate
AUTOMOUNT_SCRIPT=/opt/scripts/mount.sh

#Script used to send the notification, it have to accept the following parameters:
# $1: Identifier name
# $2: COMPLETED / ERROR
# $3: Subject
# $4: Message
NOTIFICATION_SCRIPT=/opt/scripts/notifyCSV.sh

#If enabled, each actions is reported on the console
LOG=true

####################
# Functions        #
####################
function notificationError
{
	if [ ! -z $NOTIFICATION_SCRIPT ]; then
		[ $LOG == true ] && echo "Error: $1 - $2"
		$NOTIFICATION_SCRIPT "$IDENTIFIER_NAME" "ERROR" "$1" "$2"
	else
        	echo "Notification script is empty. No notification will be sent"
	fi
}
function notificationCompleted
{
	if [ ! -z $NOTIFICATION_SCRIPT ]; then
		[ $LOG == true ] && printf "Completed: $1"
                $NOTIFICATION_SCRIPT "$IDENTIFIER_NAME" "COMPLETED" "Backup created" "$1"
		
		if [ $? -ne 0 ]; then
		        echo "Error sending the notification"
		        exit 5
		fi

        else
                echo "Notification script is empty. No notification will be sent"
        fi
}
function notificationCompletedWithError
{
        if [ ! -z $NOTIFICATION_SCRIPT ]; then
                [ $LOG == true ] && printf "Completed: $1"
                $NOTIFICATION_SCRIPT "$IDENTIFIER_NAME" "ERROR" "Backup created with errors" "$1"

                if [ $? -ne 0 ]; then
                        echo "Error sending the notification"
                        exit 5
                fi

        else
                echo "Notification script is empty. No notification will be sent"
        fi
}

####################
# Process          #
####################

#Check PostgreSQL is started
[ $LOG == true ] && echo "Checking that PostgreSQL is started"
nc -z -v -w5 localhost $POSTGRESQL_PORT > /dev/null 2> /dev/null
if [ $? -ne 0 ]; then
	notificationError "PostgreSQL is stopped" "No services is found in the port $POSTGRESQL_PORT, it may be stopped"
	exit 1
fi

#Automount
if [ ! -z $AUTOMOUNT_SCRIPT ]; then
	[ $LOG == true ] && echo "Checking if the folder is mounted"

	$AUTOMOUNT_SCRIPT
	if [ $? -ne 0 ]; then
		notificationError "Automount failed" "Automount script has failed, check manually if this works "
		exit 2
	fi
fi

#Check target folder
[ $LOG == true ] && echo "Checking backup folder"

if [ ! -d "$BACKUP_FOLDER" ]; then
	[ $LOG == true ] && echo "Creating backup folder"
	mkdir -p $BACKUP_FOLDER
	if [ $? -ne 0 ]; then
                notificationError "Backup folder doesn't exist" "The folder where to store the backup does not exist and could not be created"
                exit 3
        fi
fi

#Rotate folders
if [ ! -z $BACKUP_ROTATION_SCRIPT ]; then
	[ $LOG == true ] && echo "Rotating old backups"
	finalFolder=$($BACKUP_ROTATION_SCRIPT $BACKUP_FOLDER $BACKUP_MAXIMUM_FILES)
else
	[ $LOG == true ] && echo "Rotation disabled"
	$finalFolder=$BACKUP_FOLDER
fi

[ $LOG == true ] && echo "Final folder: $finalFolder"

#Create backup
notificationMessage="Target files:"
error=false

for database in $POSTGRESQL_DATABASES
do
	backupFilename="$finalFolder/$database$BACKUP_FILENAME_DATE.sql"
	[ $LOG == true ] && echo "Creating backup of $database on $backupFilename"
	
	su - $POSTGRESQL_USER -c "pg_dump -w $database" > $backupFilename

	if [ $? -ne 0 ]; then
		notificationMessage="$notificationMessage\nERROR $backupFilename"
		error=true
	else
		notificationMessage="$notificationMessage\n$backupFilename"
	fi
done

#Notification
[ $LOG == true ] && echo "Sending notification"

if [ $error == true ]; then
	notificationCompletedWithError "$notificationMessage"
	exit 4
else
	notificationCompleted "$notificationMessage"
fi
