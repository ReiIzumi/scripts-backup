#!/bin/bash
#
# CopyLeft moon.cat
# https://creativecommons.org/licenses/by/4.0/
#
# Author: Rei Izumi - moon.cat - 2020
#
# Rotate folders by deleting older ones
#
# $1: Folder
# $2: Maximum folders
#

###############
# PROCESS     #
###############

_mask=
function createMask
{
	_mask=$1
	if [ ${#1} -eq 1 ]; then
        	_mask=0$1
	fi
	return $mask
}

#Assign name for last folder to delete
createMask $2
finalFolder=$_mask

_last=
for ((x=$2; x>0; x--))
do
	createMask $x
	f=$_mask

	if [ -d "$1/$f" ]; then
		if [ $f -eq $finalFolder ]; then
			#Delete last folder
			rm -rf $1/$f
		else
			#Move to next old folder
			mv $1/$f $1/$_last
		fi
	fi
	_last=$f
done

#Create new folder
mkdir $1/01
echo $1/01

