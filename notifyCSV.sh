#!/bin/bash
#
# CopyLeft moon.cat
# https://creativecommons.org/licenses/by/4.0/
#
# Author: Rei Izumi - moon.cat - 2020
#
# Generates a CSV with # for each notification.
# Extension is the state (COMPLETED or ERROR). The oldest are removed.
#
# $1: Identifier name
# $2: COMPLETED / ERROR
# $3: Subject
# $4: Message
#
# Exit codes:
# -1: Notification folder doesn't exist and it's not possible to create it
#

###############
# CONFIG      #
###############

#Root folder to write the notifications
NOTIFICATION_FOLDER=/opt/scripts/notification

#Delete notifications more than X days ago
DELETE_NOTIFICATIONS_DAYS=7

####################
# Process          #
####################

#Check notification folder
folder=$NOTIFICATION_FOLDER/$1
if [ ! -d $folder ]; then
	mkdir -p $folder
	if [ $? -ne 0 ]; then
		exit 1
	fi
fi

#Write notification
date=`date +_%Y-%m-%d_%H-%M-%S`

echo "$3#$4" > $folder/$1$date.$2

find $folder -mtime +$DELETE_NOTIFICATIONS_DAYS -exec rm {} \;
