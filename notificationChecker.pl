#!/usr/bin/perl
#
# CopyLeft moon.cat
# https://creativecommons.org/licenses/by/4.0/
#
# Author: Rei Izumi - moon.cat - 2020
#
# NAGIOS SCRIPT
# Read the nofitication files generated by notifyCSV.sh script 
# and processes that data according to the output of Nagios.
# If a notification file is not found between the margins, 
# a warning or critical is returned.
#
# This script requires having PERL installed with the following modules:
# - DateTime
# - DateTime::Format::Strptime
#
# List of arguments. All are required:
# $ARGV[0]: Identifier Name (Text)
# $ARGV[1]: Start time. Format: "hh:mm:ss" (Text)
# $ARGV[2]: Minutes for warning (Number)
# $ARGV[3]: Minutes for critical (Number)
#
# Exit Codes:
# - 0: OK
# - 1: WARNING
# - 2: CRITICAL
# - 3: UNKNOWN


###############
# CONFIG      #
###############

#Folder where the notification files are stored according to the same configuration of notifyCSV.sh
my $NOTIFICATION_FOLDER="/opt/scripts/notification";

#Enable the log according: 0=false, 1=true
#Should only be used for testing, Nagios does not allow these texts
my $LOG=0;

###############
# PROCESS     #
###############

use strict;
use warnings;
use DateTime;
use DateTime::Format::Strptime qw( );

#Check that all parameters exist and prepare it
#- Check arguments
if( @ARGV != 4) {
	unknown("Arguments are missing. Required: 'Identifier Name' 'Start Time' 'Minutes for warning' 'Minutes for critical'");
}

my $nowDate = DateTime->now;
$nowDate->set_time_zone('local');
logDate("NowDate", $nowDate);

#- Create StartTime
my $format = DateTime::Format::Strptime->new(
   pattern   => '%H:%M:%S',
   time_zone => 'local'
);
my $sdFormat = $format->parse_datetime($ARGV[1]);

my $startTime = DateTime->now;
$startTime->set_time_zone('local');
$startTime->set(
        hour    => $sdFormat->hour(),
        minute  => $sdFormat->minute(),
        second  => $sdFormat->second()
);

logDate("StartTime", $startTime);

#- Create WarningTime
my $warningTime = $startTime->clone();
$warningTime->add(
	minutes	=> $ARGV[2]
);
logDate("WarningTime", $warningTime);

#- Create CriticalTime
my $criticalTime = $startTime->clone();
$criticalTime->add(
	minutes	=> $ARGV[3]
);
logDate("CriticalTime", $criticalTime);

#Check if the notification folder exist
my $folder="$NOTIFICATION_FOLDER/$ARGV[0]";

if( ! -d $folder) {
	unknown("Notification folder doesn't exist: $folder");
}

#Retrieve the newest file
opendir(my $dh, $folder);
my @files = map { [ stat "$folder/$_", $_ ] } grep(! /^\.\.?$/, readdir($dh));
closedir($dh);

sub rev_by_date { $b->[9] <=> $a->[9] }
my @sorted_files = sort rev_by_date @files;

my @newest = @{$sorted_files[0]};
my $file = $folder."/".pop(@newest);
my $fileDate = DateTime->from_epoch(
	epoch		=> (stat($file))[9],
	time_zone	=> 'local'
);

logDate("FileDate", $fileDate);

#Check the newest file according to start date and margins
#- FileDate >= StartTime:					Read file
#- FileDate < StartTime && StartTime < Now < WarningTime:       Read old file
#- FileDate < StartTime && WarningTime < Now < CriticalTime:    Warning
#- FileDate < StartTime && CriticalTime < Now:                  Critical
if(DateTime->compare($fileDate, $startTime) >= 0) {
	readFile($file);
}
else {
	if(DateTime->compare($nowDate, $warningTime) == -1 ) {
                readFile($file);
        } elsif (DateTime->compare($nowDate, $criticalTime) == 1) {
                critical("Notification file not found");
        } else {
                warning("Notification file not found");
        }
}

#Process to read the type and content of the file
sub readFile {
	debug("Read file $_[0]");
	
	#- Get content
	open(FILE, $_[0]);
	my $output = do {local $/; <FILE> };
	close(FILE);
	
	#- Get extension
	my ($ext) = $_[0] =~ /\.(.*?)$/;
	
	if(! defined $ext) {
		critical("A file was found but it does not have any extension, is not valid. $_[0]");
	} elsif($ext eq "COMPLETED") {
		ok($output);
	} elsif ($ext eq "ERROR") {
		critical($output);
	} else {
		critical("The file extension is invalid. $_[0]");
	}
}

#Logs
sub debug {
	if($LOG) {
		print $_[0]."\n";
	}
}
sub logDate {
	debug("$_[0]:\t".$_[1]->iso8601()." ".$_[1]->time_zone_short_name());
}

#Exit Codes
sub ok {
	exitCode("OK", $_[0], 0);
}
sub warning {
	exitCode("WARNING", $_[0], 1);
}
sub critical {
	exitCode("CRITICAL", $_[0], 2);
}
sub unknown {
	exitCode("UNKNOWN", $_[0], 3);
}
sub exitCode {
	print "$_[0] - $_[1]";
	exit $_[2];
}
