#!/bin/bash
#
# CopyLeft moon.cat
# https://creativecommons.org/licenses/by/4.0/
#
# Author: Rei Izumi - moon.cat - 2020
#
# Generates a backup of the chosen file or folder.
#
# Exit codes:
# - 1: Source file or folder not found
# - 2: Error mounting the folder
# - 3: Target folder doesnt' exist and it's not possible to create it
# - 4: Value of BACKUP_COMPRESSION_LEVEL is not correct
# - 5: Value of BACKUP_COMPRESSION_TYPE is not correct
# - 6: Error sending the notification 

####################
# Config           #
####################

#Process identifier used by notification
IDENTIFIER_NAME=TestBckp

#Folder or file to create the backup
SOURCE_FOLDER=/mnt/backup/test

#Destination folder for the backup
BACKUP_FOLDER=/mnt/backup/$IDENTIFIER_NAME

#Number of backups to store.
#If the script path is empty, rotation will be disabled
BACKUP_MAXIMUM_FILES=5
BACKUP_ROTATION_SCRIPT=/opt/scripts/rotateFiles.sh

#true: Compress each file or folder into different files
#false: Compress the folder into a single file
BACKUP_SEPARATE_FILES=true

#Compression type between (in order of fastest to best compression): gzip, bzip2 or xz
#The O.S. must have the process installed
BACKUP_COMPRESSION_TYPE=gzip

#Compression value from fastest to best compression: 1 to 9
BACKUP_COMPRESSION_LEVEL=1

#Date format used in the backup filename
BACKUP_FILENAME_DATE=`date +_%Y-%m-%d_%H-%M-%S`

#Script to mount the unit at the beginning of the process
#Do not indicate any value to deactivate
AUTOMOUNT_SCRIPT=/opt/scripts/mount.sh

#Script used to send the notification, it have to accept the following parameters:
# $1: Identifier name
# $2: COMPLETED / ERROR
# $3: Subject
# $4: Message
NOTIFICATION_SCRIPT=/opt/scripts/notifyCSV.sh

#If enabled, each actions is reported on the console
LOG=true

####################
# Functions        #
####################
function notificationError
{
	if [ ! -z $NOTIFICATION_SCRIPT ]; then
		[ $LOG == true ] && echo "Error: $1 - $2"
		$NOTIFICATION_SCRIPT "$IDENTIFIER_NAME" "ERROR" "$1" "$2"
	else
        	echo "Notification script is empty. No notification will be sent"
	fi
}
function notificationCompleted
{
	if [ ! -z $NOTIFICATION_SCRIPT ]; then
		[ $LOG == true ] && printf "Completed: $1"
                $NOTIFICATION_SCRIPT "$IDENTIFIER_NAME" "COMPLETED" "Backup created" "$1"
		
		if [ $? -ne 0 ]; then
		        echo "Error sending the notification"
		        exit 6
		fi

        else
                echo "Notification script is empty. No notification will be sent"
        fi
}

function compressGzip
{
        processedTime=$(time ( GZIP=-$BACKUP_COMPRESSION_LEVEL tar czf $1 $2 2>/dev/null 1>&2 ) 2>&1)
}
function compressBzip2
{
	processedTime=$(time ( BZIP2=-$BACKUP_COMPRESSION_LEVEL tar cjf $1 $2 2>/dev/null 1>&2 ) 2>&1)
}
function compressXz
{
	processedTime=$(time ( XZ_OPT=-$BACKUP_COMPRESSION_LEVEL tar -Jcf $1 $2 2>/dev/null 1>&2 ) 2>&1)
}

####################
# Process          #
####################

#Check source folder
[ $LOG == true ] && echo "Checking the source folder"

if [ ! -f "$SOURCE_FOLDER" -a ! -d "$SOURCE_FOLDER" ]; then
	notificationError "sourceFolder not found" "You must configure a file or folder to be stored"
	exit 1
fi

#Automount
if [ ! -z $AUTOMOUNT_SCRIPT ]; then
	[ $LOG == true ] && echo "Checking if the folder is mounted"

	$AUTOMOUNT_SCRIPT
	if [ $? -ne 0 ]; then
		notificationError "Automount failed" "Automount script has failed, check manually if this works "
		exit 2
	fi
fi

#Check target folder
[ $LOG == true ] && echo "Checking backup folder"

if [ ! -d "$BACKUP_FOLDER" ]; then
	[ $LOG == true ] && echo "Creating backup folder"
	mkdir -p $BACKUP_FOLDER
	if [ $? -ne 0 ]; then
                notificationError "Backup folder doesn't exist" "The folder where to store the backup does not exist and could not be created"
                exit 3
        fi
fi

#Rotate folders
if [ ! -z $BACKUP_ROTATION_SCRIPT ]; then
	[ $LOG == true ] && echo "Rotating old backups"
	finalFolder=$($BACKUP_ROTATION_SCRIPT $BACKUP_FOLDER $BACKUP_MAXIMUM_FILES)
else
	[ $LOG == true ] && echo "Rotation disabled"
	$finalFolder=$BACKUP_FOLDER
fi

[ $LOG == true ] && echo "Final folder: $finalFolder"

#Prepare function according to the chosen compression
if [ $BACKUP_COMPRESSION_LEVEL -lt 1 -o $BACKUP_COMPRESSION_LEVEL -gt 9 ]; then
	notificationError "Compression level incorrect" "The value of compression level must be between 1 (fast) or 9 (better compression)"
	exit 4
fi

case $BACKUP_COMPRESSION_TYPE in
	"gzip")
		[ $LOG == true ] && echo "Using gzip"
		function="compressGzip"
		extension=".tar.gz"
		;;
	"bzip2")
		[ $LOG == true ] && echo "Using bzip2"
                function="compressBzip2"
                extension=".tar.bz2"

		;;
	"xz")
		[ $LOG == true ] && echo "Using xz"
                function="compressXz"
                extension=".tar.xz"
		;;
	*)
		notificationError "Compression type incorrect" "The type of compression must be 'gzip', 'bzip2' or 'xz'"
		exit 5
		;;
esac

#Create backup
notificationMessage=

TIMEFORMAT=%R
if [ $BACKUP_SEPARATE_FILES == true ]; then
	cd $SOURCE_FOLDER
	notificationMessage="Target files:"
	for i in *; do
		backupFilename="$finalFolder/$i$BACKUP_FILENAME_DATE$extension"
		[ $LOG == true ] && echo "Compressing $SOURCE_FOLDER/$i into $backupFilename"
		${function} "$backupFilename" "$SOURCE_FOLDER/$i"
		echo "Processed in $processedTime s"
		notificationMessage="$notificationMessage\n$backupFilename ($processedTime s)"
	done
else
	backupFilename="$finalFolder/$IDENTIFIER_NAME$BACKUP_FILENAME_DATE$extension"
	[ $LOG == true ] && echo "Compressing $SOURCE_FOLDER into $backupFilename"
	${function} "$backupFilename" "$SOURCE_FOLDER"
	echo "Processed in $processedTime s"
	notificationMessage="Target file: $backupFilename ($processedTime s)"
fi

#Notification
[ $LOG == true ] && echo "Sending notification"
notificationCompleted "$notificationMessage"

